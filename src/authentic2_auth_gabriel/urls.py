from django.conf.urls import patterns, url

from authentic2.decorators import setting_enabled, required

from . import app_settings
from .views import token_login

urlpatterns = required(
    setting_enabled('front_enable', settings=app_settings),
    patterns(
        '',
        url('^authentic2_auth_gabriel/$', token_login,
            name='authentic2-auth-gabriel-index'),
    )
)
