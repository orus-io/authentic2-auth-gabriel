from django.shortcuts import redirect
from django.contrib.auth import login

from . import backends


def token_login(request):
    b = backends.GabrielBackend()
    user = b.authenticate(
        request=request,
        username=request.GET.get('userid'),
        password=request.GET.get('token'),
        is_token_validation=True
    )

    if user is not None:
        user.backend = 'authentic2_auth_gabriel.backends.GabrielBackend'
        login(request, user)

    return redirect('/')
