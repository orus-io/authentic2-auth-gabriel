from django.test import TestCase
from django.http import HttpRequest
from django.contrib.auth import get_user_model

import responses

from . import backends
from . import app_settings
from . import test_samples

User = get_user_model()


def success_login_callback(request):
    if '<connect>' in request.body.content:
        return (200, {}, test_samples.CONNECT_OK)
    else:
        return (200, {}, test_samples.LOGIN_OK)


def fail_login_callback(request):
    if '<connect>' in request.body.content:
        return (200, {}, test_samples.CONNECT_OK)
    else:
        return (200, {}, test_samples.LOGIN_FAIL)


def fail_login_pwd_callback(request):
    if '<connect>' in request.body.content:
        return (200, {}, test_samples.CONNECT_OK)
    else:
        return (200, {}, test_samples.LOGIN_FAIL_PWD)


def fail_connect_callback(request):
    if '<connect>' in request.body.content:
        return (200, {}, test_samples.CONNECT_FAIL)
    else:
        # return ok here but it should not be called
        # and backend should return no user because
        # connection failed
        return (200, {}, test_samples.LOGIN_OK)


class TestBackend(TestCase):

    def get_request(self):
        r = HttpRequest()
        r.POST['username'] = "john_doe"
        r.POST['password'] = "secret"
        r.method = "POST"
        return r

    @responses.activate
    def test_success_login(self):

        responses.add_callback(
            responses.POST,
            app_settings.backend_url,
            callback=success_login_callback)

        b = backends.GabrielBackend()

        res = b.authenticate(self.get_request())

        self.assertTrue(isinstance(res, User))

    @responses.activate
    def test_fail_login(self):

        responses.add_callback(
            responses.POST,
            app_settings.backend_url,
            callback=fail_login_callback)

        b = backends.GabrielBackend()

        res = b.authenticate(self.get_request())

        self.assertFalse(isinstance(res, User))

    @responses.activate
    def test_fail_login_pwd(self):

        responses.add_callback(
            responses.POST,
            app_settings.backend_url,
            callback=fail_login_pwd_callback)

        b = backends.GabrielBackend()

        res = b.authenticate(self.get_request())

        self.assertFalse(isinstance(res, User))

    @responses.activate
    def test_connect_fail(self):

        responses.add_callback(
            responses.POST,
            app_settings.backend_url,
            callback=fail_connect_callback)

        b = backends.GabrielBackend()

        res = b.authenticate(self.get_request())

        self.assertFalse(isinstance(res, User))
