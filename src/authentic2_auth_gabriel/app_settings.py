import sys


class AppSettings(object):
    __DEFAULTS = {
        'ENABLE': True,
    }

    def __init__(self, prefix):
        self.prefix = prefix

    def _setting(self, name, dflt):
        from django.conf import settings
        return getattr(settings, self.prefix + name, dflt)

    def __getattr__(self, name):
        if name not in self.__DEFAULTS:
            raise AttributeError(name)
        return self._setting(name, self.__DEFAULTS[name])

    @property
    def enable(self):
        return self._setting('ENABLE', False)

    @property
    def front_enable(self):
        return self._setting('FRONT_ENABLE', False)

    @property
    def about_url(self):
        return self._setting('ABOUT_URL', 'https://www.ec-gabriel.fr/')

    @property
    def backend_url(self):
        return self._setting(
            'BACKEND_URL',
            'https://www.ec-gabriel.fr/wssecure/')

    @property
    def ssl_verify(self):
        '''You can either set this parameter to Flase to skip SSL verify
        or set a absolute path to SSL certificates'''
        return self._setting('SSL_VERIFY', True)

    @property
    def client_id(self):
        return self._setting('CLIENT_ID', '')

    @property
    def client_password(self):
        return self._setting('CLIENT_PASSWORD', '')

# Ugly? Guido recommends this himself ...
# http://mail.python.org/pipermail/python-ideas/2012-May/014969.html
app_settings = AppSettings('A2_GABRIEL_')
app_settings.__name__ = __name__
sys.modules[__name__] = app_settings
