#!/usr/bin/env python
# -*- coding: utf-8 -*-
# flake8:noqa

CONNECT_OK = '''
<?xml version="1.0" encoding="UTF-8"?>
<env:Envelope xmlns:env="http://www.w3.org/2003/05/soap-envelope" xmlns:ns1="http://gabriel-preprod-4.gabriel.hosting.enovance.com/wssecure/?wsdl" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:enc="http://www.w3.org/2003/05/soap-encoding">
    <env:Body xmlns:rpc="http://www.w3.org/2003/05/soap-rpc">
        <ns1:connectResponse env:encodingStyle="http://www.w3.org/2003/05/soap-encoding">
            <rpc:result>return</rpc:result>
            <return xsi:type="xsd:string">Authentification réussie</return>
        </ns1:connectResponse>
    </env:Body>
</env:Envelope>
'''

CONNECT_FAIL = '''
<?xml version="1.0" encoding="UTF-8"?>
<env:Envelope xmlns:env="http://www.w3.org/2003/05/soap-envelope" xmlns:ns1="http://gabriel-preprod-4.gabriel.hosting.enovance.com/wssecure/?wsdl" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:enc="http://www.w3.org/2003/05/soap-encoding">
    <env:Body xmlns:rpc="http://www.w3.org/2003/05/soap-rpc">
        <ns1:connectResponse env:encodingStyle="http://www.w3.org/2003/05/soap-encoding">
            <rpc:result>return</rpc:result>
            <return xsi:type="xsd:string">authentification.erreur.message.identifiant.incorrects</return>
        </ns1:connectResponse>
    </env:Body>
</env:Envelope>
'''

LOGIN_OK = '''
<?xml version="1.0" encoding="UTF-8"?>
<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="http://gabriel-preprod-4.gabriel.hosting.enovance.com/wssecure/?wsdl" xmlns:ns2="http://xml.apache.org/xml-soap" xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/" xmlns:xsi="h ttp://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
    <SOAP-ENV:Body>
        <ns1:loginResponse>
            <return SOAP-ENC:arrayType="ns2:Map[1]" xsi:type="SOAP-ENC:Array">
                <item xsi:type="ns2 :Map">
                    <item>
                        <key xsi:type="xsd:string">login</key>
                        <value xsi:type="xsd:string">kcouhaut</value>
                    </item>
                    <item>
                        <key xsi:type="xsd:string">numec</key>
                        <value xsi:type="xsd:string">29426966233</value>
                    </item>
                    <item>
                        <key xsi:type="xsd:string">num_entree_id</key>
                        <value xsi:type="xsd :string">0860959A000V1CJ5KS4E</value>
                    </item>
                    <item>
                        <key xsi:type="xsd:string">code_rne</key>
                        <value xsi:type="xsd:string"></value>
                    </item>
                    <item>
                        <key xsi:type="xsd:string">nom</key>
                        <value xsi:type="xsd:string">COUHAUT</value>
                    </item>
                    <item>
                        <key xsi:type="xsd:string">prenom_usuel</key>
                        <value xsi:type="xsd:string">Karine</value>
                    </item>
                    <item>
                        <key xsi:type="xsd:string">mail</key>
                        <value xsi:type="xsd:string">kari.co@foo.io</value>
                    </item>
                    <item>
                        <key xsi:type="xsd:string">id_fonction</key>
                        <value xsi:type="xsd:int">38</value>
                    </item>
                    <item>
                        <key xsi:type="x sd:string">fonction</key>
                        <value xsi:type="xsd:string">Enseignant(e)</value>
                    </item>
                    <item>
                        <key xsi:type="xsd:string">structure_usage</key>
                        <value xsi:nil="true" />
                    </item>
                    <item>
                        <key xsi:type="xsd:string">structure_organisme</key>
                        <value xsi:nil="true" />
                    </item>
                    <item>
                        <key xsi:type="xsd:string">structure_numec</key>
                        <value xsi:nil="true" />
                    </item>
                    <item>
                        <key xsi:type="xsd:string">id_diocese</key>
                        <value xsi:nil="true" />
                    </item>
                    <item>
                        <key xsi:type="xsd:string">diocese</key>
                        <value xsi:type="xsd:string"></value>
                    </item>
                    <item>
                        <key xsi:type="xsd:string">id_academie</key>
                        <value xsi:nil="true" />
                    </item>
                    <item>
                        <key xsi:type="xsd:string">academie</key>
                        <value xsi:type="xsd:string"></value>
                    </item>
                    <item>
                        <key xsi:type="xsd:string">mdp</key>
                        <value xsi:type="xsd:string">secret</value>
                    </item>
                    <item>
                        <key xsi:type="xsd:string">date_naissance</key>
                        <value xsi:type="xsd:string">1978-04-29</value>
                    </item>
                </item>
            </return>
        </ns1:loginResponse>
    </SOAP-ENV:Body>
</SOAP-ENV:Envelope>
'''

LOGIN_FAIL = '''
<?xml version="1.0" encoding="UTF-8"?>
<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="http://gabriel-preprod-4.gabriel.hosting.enovance.com/wssecure/?wsdl" xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
    <SOAP-ENV:Body>
        <ns1:loginResponse>
            <return SOAP-ENC:arrayType="xsd:ur-type[0]" xsi:type="SOAP-ENC:Array" />
        </ns1:loginResponse>
    </SOAP-ENV:Body>
</SOAP-ENV:Envelope>
'''

LOGIN_FAIL_PWD = '''
<?xml version="1.0" encoding="UTF-8"?>
<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="http://gabriel-preprod-4.gabriel.hosting.enovance.com/wssecure/?wsdl" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/" SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
    <SOAP-ENV:Body>
        <ns1:loginResponse>
            <return xsi:type="xsd:string">erreurMDP</return>
        </ns1:loginResponse>
    </SOAP-ENV:Body>
</SOAP-ENV:Envelope>
'''
