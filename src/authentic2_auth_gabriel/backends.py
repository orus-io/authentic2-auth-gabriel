#!/usr/bin/env python # -*- coding: utf-8 -*-
import uuid
from django.contrib.auth import get_user_model
from django.contrib.auth.backends import ModelBackend
from django.shortcuts import render
from django.http import HttpRequest

import requests
import lxml.html

from . import app_settings
from . import models

User = get_user_model()


class GabrielBackend(ModelBackend):

    def authenticate(self, *args, **kwargs):
        '''Return a GabrielAccount instance on success else None
        '''

        self.request = kwargs.get('request', HttpRequest())
        username = kwargs.get('username')
        password = kwargs.get('password')

        if username is None or password is None:
            return None

        is_token_validation = kwargs.get('is_token_validation')

        with requests.Session() as session:

            success = self._connect(session)
            if not success:
                return None

            if is_token_validation is None:
                res = self._login(session, username, password)
                # Try a token validation, in case `is_token_validation`
                # is just forgetten
                if not res:
                    res = self._login(
                        session, username, password, validate=True)
                if not res:
                    return None
            else:
                res = self._login(session, username, password, validate=True)
                if not res:
                    return None

        # numec is the only "credible" parameter available
        if not res or 'numec' not in res or not res['numec']:
            return None

        try:
            account = models.GabrielAccount.objects.get(
                numec=res['numec'], user__is_active=True)

            return account.user

        except models.GabrielAccount.DoesNotExist:

            user = User.objects.create(
                username=uuid.uuid4(),
                first_name=res.pop('nom', ''),
                last_name=res.pop('prenom_usuel', ''))

            # remove not implemented fields
            f_names = [
                f.name
                for f in models.GabrielAccount._meta.get_fields()
            ]
            res = {k: v for k, v in res.items() if k in f_names}

            account = models.GabrielAccount.objects.create(
                user=user, **res)

            return user

    def _connect(self, session):
        payload = render(
            self.request, 'authentic2_auth_gabriel/connect.xml',
            {
                'login': app_settings.client_id,
                'password': app_settings.client_password,
            })

        res = session.post(
            app_settings.backend_url,
            data=payload, verify=app_settings.ssl_verify)

        # reponse always return a 200 even on fail
        etree = lxml.html.fromstring(res.text.encode('utf-8'))
        items = etree.xpath('//return')
        if items and \
                u'Authentification r\xe9ussie' \
                not in items[0].text:
            return False

        return True

    def _login(self, session, username, password, validate=False):
        '''Login method:
        - @username: username or user_id, depending if you do real login or
                     a token validation
        - @password: password or token, depending if you do real login or
                     a token validation
        - @validate: boolean, if false you are query for login else for
                     token validation
        '''
        if not validate:
            payload = render(
                self.request, 'authentic2_auth_gabriel/login.xml',
                {
                    'url': app_settings.backend_url,
                    'login': username,
                    'password': password,
                })
        else:
            payload = render(
                self.request, 'authentic2_auth_gabriel/validate.xml',
                {
                    'url': app_settings.backend_url,
                    'user_id': username,
                    'token': password,
                })

        res = session.post(
            app_settings.backend_url,
            data=payload, verify=app_settings.ssl_verify)

        # reponse always return a 200 even on fail
        etree = lxml.html.fromstring(res.text.encode('utf-8'))

        try:
            res = {
                v.xpath('//key')[i].text.lower(): v.xpath('//value')[i].text
                for i, v in enumerate(etree.xpath('//item/item'))
            }
        except IndexError:  # malformed response
            return False

        return res

    def get_saml2_authn_context(self):
        import lasso
        return lasso.SAML2_AUTHN_CONTEXT_PASSWORD_PROTECTED_TRANSPORT
