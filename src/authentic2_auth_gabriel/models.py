from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.conf import settings


class GabrielAccount(models.Model):
    user = models.ForeignKey(
        to=settings.AUTH_USER_MODEL,
        verbose_name=_('user'),
        related_name='gabriel_accounts')

    # all the following fields came from gabriel
    # all charfield length are larger is case of API change
    numec = models.CharField(max_length=512, db_index=True)
    login = models.CharField(max_length=512, default='')
    num_entree_id = models.CharField(max_length=512, default='')
    nom = models.CharField(max_length=512, default='')
    prenom_usuel = models.CharField(max_length=512, default='')
    mail = models.CharField(max_length=512, default='')
    id_fonction = models.CharField(max_length=512, default='')
    fonction = models.CharField(max_length=512, default='')
