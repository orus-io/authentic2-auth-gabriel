# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='GabrielAccount',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('numec', models.CharField(max_length=30, db_index=True)),
                ('num_entree_id', models.CharField(default=b'', max_length=100)),
                ('id_fonction', models.IntegerField(default=0)),
                ('fonction', models.CharField(default=b'', max_length=512)),
                ('id_academie', models.IntegerField(default=0)),
                ('academie', models.CharField(default=b'', max_length=512)),
                ('id_diocese', models.IntegerField(default=0)),
                ('diocese', models.CharField(default=b'', max_length=512)),
                ('user', models.ForeignKey(related_name='gabriel_accounts', verbose_name='user', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
