# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('authentic2_auth_gabriel', '0002_auto_20170713_1943'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='gabrielaccount',
            name='email',
        ),
        migrations.RemoveField(
            model_name='gabrielaccount',
            name='nom_complet',
        ),
        migrations.RemoveField(
            model_name='gabrielaccount',
            name='prenom',
        ),
        migrations.RemoveField(
            model_name='gabrielaccount',
            name='structure_rattachement',
        ),
        migrations.AddField(
            model_name='gabrielaccount',
            name='fonction',
            field=models.CharField(default=b'', max_length=512),
        ),
        migrations.AddField(
            model_name='gabrielaccount',
            name='id_fonction',
            field=models.CharField(default=b'', max_length=512),
        ),
        migrations.AddField(
            model_name='gabrielaccount',
            name='login',
            field=models.CharField(default=b'', max_length=512),
        ),
        migrations.AddField(
            model_name='gabrielaccount',
            name='mail',
            field=models.CharField(default=b'', max_length=512),
        ),
        migrations.AddField(
            model_name='gabrielaccount',
            name='num_entree_id',
            field=models.CharField(default=b'', max_length=512),
        ),
        migrations.AddField(
            model_name='gabrielaccount',
            name='prenom_usuel',
            field=models.CharField(default=b'', max_length=512),
        ),
    ]
