# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('authentic2_auth_gabriel', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='gabrielaccount',
            name='academie',
        ),
        migrations.RemoveField(
            model_name='gabrielaccount',
            name='diocese',
        ),
        migrations.RemoveField(
            model_name='gabrielaccount',
            name='fonction',
        ),
        migrations.RemoveField(
            model_name='gabrielaccount',
            name='id_academie',
        ),
        migrations.RemoveField(
            model_name='gabrielaccount',
            name='id_diocese',
        ),
        migrations.RemoveField(
            model_name='gabrielaccount',
            name='id_fonction',
        ),
        migrations.RemoveField(
            model_name='gabrielaccount',
            name='num_entree_id',
        ),
        migrations.AddField(
            model_name='gabrielaccount',
            name='email',
            field=models.CharField(default=b'', max_length=512),
        ),
        migrations.AddField(
            model_name='gabrielaccount',
            name='nom',
            field=models.CharField(default=b'', max_length=512),
        ),
        migrations.AddField(
            model_name='gabrielaccount',
            name='nom_complet',
            field=models.CharField(default=b'', max_length=512),
        ),
        migrations.AddField(
            model_name='gabrielaccount',
            name='prenom',
            field=models.CharField(default=b'', max_length=512),
        ),
        migrations.AddField(
            model_name='gabrielaccount',
            name='structure_rattachement',
            field=models.CharField(default=b'', max_length=512),
        ),
        migrations.AlterField(
            model_name='gabrielaccount',
            name='numec',
            field=models.CharField(max_length=512, db_index=True),
        ),
    ]
