# authentic2 auth gabriel

## Install

You must first have an founctionnal Authentic2 install, then install this
plugin using pip.

## Settings

- `A2_GABRIEL_ENABLE`: enable this plugin
- `A2_GABRIEL_FRONT_ENABLE`: enable the front end for token authentification
- `A2_GABRIEL_ABOUT_URL`: deprecated
- `A2_GABRIEL_BACKEND_URL`: url to Gabriel login API
- `A2_GABRIEL_SSL_VERYFY`: can be a boolean to set SSL verify, or absolute path to an SSL certificate.
- `A2_GABRIEL_CLIENT_ID`: Gabriel API client id
- `A2_GABRIEL_CLIENT_PASSWORD`: Gabriel API client password

you must also add `authentic2_auth_gabriel.backends.GabrielBackend` to the AUTHENTICATION_BACKENDS list in you `settings.py`

## Token authentification

Token authentification is done by using the following endpoint:

```
https://<your-url>/authentic2_auth_gabriel
```

It requires two parameters named `userid` and `token`, those are passed using query parameters, eg.:

```
https://<your-url>/authentic2_auth_gabriel?userid=michel&token=secret
```

On success, logged in user will be redirected to the root page.

On fail, unlogged user will be redirected to the root page too, meaning that the login from will be shown.

## running tests suite

Once your install is done you can run the test suite using:

`python manage.py test authentic2_auth_gabriel`
